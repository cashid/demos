<!DOCTYPE html>
<html lang='en'>
	<head>
		<title>CashID Demo Requests</title>
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<link rel="icon" type="image/png" href="favicon.ico" />
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel='stylesheet' href='https://www.cashid.info/css/brand.css'>
		<link rel='stylesheet' href='https://demo.cashid.info/css/demo.css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<script src="/lib/qr.js"></script>
		<script src="/lib/cashid.js"></script>
	</head>
	<body>
		<section>
			<header>CashID example requests</header>
			<small>This page implements the examples listed in the specification and gives programming insight into what data is sent between the identity manager and the service provider.<br /><br />To get started, <a href='https://cashid.info/files/cashid_beta_20181003.apk'>Download the CashID app (beta)</a>, or use a wallet with CashID support.</small>

			<ul class='examples'>
<?php
	// Include the CashID support library for PHP.
	require_once('lib/cashid.php');

	$requestList =
	[
		'minimal' =>
		[
			'title' => 'Minimal authentication request',
			'description' => 'The smallest possible request only authenticates a user.',
			'action' => null,
			'data' => null,
			'metadata' => []
		],
		'login' =>
		[
			'title' => 'Logging in to a website',
			'description' => 'The login action can use Nickname to update the user profile.',
			'action' => 'login',
			'data' => '15366-4133-6141-9638',
			'metadata' => 
			[
				'optional' => 
				[
					'identity' => [ 'nickname' ]
				]
			]
		],
		'register' =>
		[
			'title' => 'Signing up for a newsletter',
			'description' => 'The register action for the newsletter requires Name, Family, Country and Email to work, and will use Age, Gender and City if provided.',
			'action' => 'register',
			'data' => 'newsletter',
			'metadata' => 
			[
				'required' => 
				[
					'identity' => ['name', 'family'],
					'position' => ['country'],
					'contact' => ['email']
				],
				'optional' => 
				[
					'identity' => ['age', 'gender'],
					'position' => ['city']
				]
			]
		],
		'delete' =>
		[
			'title' => 'Account removal',
			'description' => 'The request to delete their user data. This can also be initiated directly from identity managers.',
			'action' => 'delete',
			'data' => null,
			'metadata' => []
		],
		'transient' =>
		[
			'title' => 'Transient user profiles',
			'description' => 'The login action (for a service provider which as a security measure does not store user profile information) will only work if the user provides a nickname and email, and will give a better customized user experience if the user provides Country, Age, Gender and a Postal label.',
			'action' => 'login',
			'data' => 'a7fbb9341ce3ae21',
			'metadata' =>
			[
				'required' => 
				[
					'identity' => ['nickname'],
					'contact' => ['email']
				],
				'optional' => 
				[
					'identity' => ['age', 'gender'],
					'position' => ['country'],
					'contact' => ['postal']
				]
			]
		]
	];

	foreach($requestList as $requestName => $requestData)
	{
		// Create the minimal request
		$requestURI = $cashid->create_request($requestData['action'], $requestData['data'], $requestData['metadata']);

		// Parse the request to example the parts.
		$requestParts = $cashid->parse_request($requestURI);

		echo "				<li class='example' data-request-uri='{$requestURI}' data-request-nonce='{$requestParts['parameters']['nonce']}'>";
		echo "					<div class='requestDescription'>";
		echo "						<header class='requestTitle'>{$requestData['title']}</header>";
		echo "						<small class='form-text text-muted requestExplanation'>{$requestData['description']}</small>";
		echo "					</div>";
		echo "					<ul class='requestParts'>";
		echo "						<li class='requestView'>";
		echo "							<header>Request Code</header>";
		echo "							<small class='form-text text-muted requestText'>{$requestURI}</small>";
		echo "							<span class='requestQR'></span>";
		echo "							<pre>123</pre>";
		echo "						</li>";
		echo "						<li class='responseJSON'>";
		echo "							<header>Response Object</header>";
		echo "							<pre></pre>";
		echo "						</li>";
		echo "						<li class='confirmationJSON'>";
		echo "							<header>Confirmation Object</header>";
		echo "							<pre></pre>";
		echo "						</li>";
		echo "					</ul>";
		echo "				</li>";
	}
?>
			</ul>
		</section>
		<script>
			window.onload = function()
			{
				function handle_response(event)
				{
					let exampleNodes = document.getElementsByClassName('example');

					for(node in exampleNodes)
					{
						if(typeof exampleNodes[node] == 'object')
						{
							if(exampleNodes[node].getAttribute('data-request-nonce') == event.target.nonce)
							{
								exampleNodes[node].getElementsByClassName('responseJSON')[0].getElementsByTagName("pre")[0].innerHTML = JSON.stringify(JSON.parse(event.data), null, 2);
							}
						}
					}

					// Print the content of the event to the webpage.
					console.log("RESPONSE");
					console.log(event.target.nonce);
					console.log(event.data);
				}

				function handle_confirmation(event)
				{
					let exampleNodes = document.getElementsByClassName('example');

					for(node in exampleNodes)
					{
						if(typeof exampleNodes[node] == 'object')
						{
							if(exampleNodes[node].getAttribute('data-request-nonce') == event.target.nonce)
							{
								exampleNodes[node].getElementsByClassName('confirmationJSON')[0].getElementsByTagName("pre")[0].innerHTML = JSON.stringify(JSON.parse(event.data), null, 2);
							}
						}
					}

					// Print the content of the event to the webpage.
					console.log("CONFIRMATION");
					console.log(event);
					console.log(event.data);
				}

				let exampleNodes = document.getElementsByClassName('example');

				for(node in exampleNodes)
				{
					if(typeof exampleNodes[node] == 'object')
					{
						let requestNonce = exampleNodes[node].getAttribute('data-request-nonce');
						let requestURI = exampleNodes[node].getAttribute('data-request-uri');

						let requestQR = exampleNodes[node].getElementsByClassName('requestQR')[0];
						let requestJSONview = exampleNodes[node].getElementsByClassName('requestView')[0].getElementsByTagName("pre")[0];

						let requestObject = parseCashIDRequest(requestURI);

						requestJSONview.innerHTML = JSON.stringify(requestObject, null, 2);

						let codeParameters = 
						{
							text: requestURI,
							width: 192,
							height: 192,
							colorDark: '#000',
							colorLight: '#fff',
							correctLevel: QRCode.CorrectLevel['L']
						};

						// FIXME: Don't throwaway the reference to the QR code... ?
						let qr = new QRCode(requestQR, codeParameters);

						// Check for SSE support.
						if(typeof(EventSource) !== "undefined")
						{
							// Open a connection to an event stream.
							let event_source = new EventSource('api/event.php?x=' + requestNonce);

							// Store the nonce on the event for easier management.
							event_source.nonce = requestNonce;
							
							// Set a function to handle stream opened status changes.
							event_source.onopen = function(event)
							{
								// Display a message stating that we opened the stream.
								console.log("Connected to event stream.");
							}

							// Set a function to handle stream error status changes.
							event_source.onerror = function(event)
							{
								// If the connection was permanently closed..
								if(event.readyState == EventSource.CLOSED)
								{
									// Display a message stating that we had an error with the connection.
									console.log("Lost the connection to the event stream.");
								}
								else
								{
									console.log("Unknown error with the stream: ");
									console.log(event);
								}
							}

							// Set a function to handle stream generic messages.
							event_source.onmessage = function(event)
							{
								// Pass the event to the display_event function.
								display_event(event);
							}

							// Add an event listener for response events..
							event_source.addEventListener('response', handle_response, false);

							// Add an event listener for response events..
							event_source.addEventListener('confirmation', handle_confirmation, false);
						}
						else
						{
							// SSE is not supported.
							console.log("The browser does not support Server Sent Events.");
						}

					}
				}
			}
		</script>
	</body>
</html>
