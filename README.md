# CashID Demos

Website at demo.cashid.info that hosts a set of CashID demo requests which can be used to test client implementations as well as showcase various use cases for the CashID authentication protocol.